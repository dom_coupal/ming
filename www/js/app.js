// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic','Indices','firebase'])

.run(function($ionicPlatform,$rootScope,$state) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
  
  $rootScope.$on('$stateChangeError',function(e,toState,toParams,fromState,fromParams,err){
    if(err=='RESTARTING'){
     $state.go('indices');
    }
  });
  
  $rootScope.$on('$stateChangeSuccess',function(e,toState,toParams,fromState,fromParams){
    
  });
  
})
.constant('FIREBASE_URL','https://minou.firebaseio.com/')
.config(function($stateProvider,$urlRouterProvider){
  $stateProvider.state('home',{
    url:'/',
    templateUrl:'views/home/home.html',
    controller:'HomeCtrl',
    resolve:{
      indiceNo:function(IndicesService,$q){
        var def=$q.defer();

        IndicesService.getLastIndice()
        .then(function(no){
          if(no>1){
            return def.reject('RESTARTING');
          }else{
            return def.resolve(no);
          }
        });
        
        return def.promise;
      }
    }
  })
  .state('indices',{
    url:'/indices',
    templateUrl:'views/indices/indices.html',
    controller:'IndicesCtrl',
    resolve:{
      indice:function(IndicesService){
        return IndicesService.getLastIndice();
      }
    }
  })
  .state('loading',{
    url:'/loading',
    controller:'LoaderCtrl',
    templateUrl:'views/loader.html'
  });
  $urlRouterProvider.otherwise('/');
})
.controller('HomeCtrl',function($scope,indiceNo,$state){
  //Localisateur.init('ming');
  if(indiceNo>1){
    $state.go('indices');
  }
})
.controller('LoaderCtrl',function($state,$timeout){
  $timeout(function(){
    $state.go('indices');
  },200);
})
.factory('IndicesService',function($q,$firebaseObject,FIREBASE_URL){
  var url=FIREBASE_URL;
  
  var actualIndice=null;
  var objIndices=null;
  return {
    getLastIndice:function(){
      
      if(actualIndice==null){
        return this.getLastIndiceFromFB().then(function(no){
          actualIndice=no;
          return no;
        });   
      }else{
        var def=$q.defer();
        def.resolve(actualIndice);
        return def.promise;
      }
    },
    getLastIndiceFromFB:function(){
       objIndices=$firebaseObject(new Firebase(url+'indices'));
       return objIndices.$loaded().then(function(d){
          if(objIndices.last==null){
            return 1;
          }else{
            return objIndices.last;
          }
        });
    },
    setNextIndice:function(){
      actualIndice++;
      objIndices.last=actualIndice;
      return objIndices.$save();
    },
    getSyncedChat:function(){
      return $firebaseObject(new Firebase(url+'chat'));
    }
  };
})
/*.factory('Localisateur',function($geofire,FIREBASE_URL,$q,$ionicPlatform,$cordovaGeolocation,$interval){
  var url=FIREBASE_URL,
      geo=$geofire(new Firebase(url+'/loc')),
      isInit=false,
      interval,
      delay=20000,
      _nom;
  
  function getCurrentPosition(){
    var def=$q.defer();
    
    $ionicPlatform.ready(function(){
      var posOptions={timeout:10000,enableHightAccuracy:false};
      $cordovaGeolocation.getCurrentPosition(posOptions)
      .then(function(position){
        def.resolve(position);
      },function(locationError){
        defer.reject({code:locationError.code,
                     message:locationError.message,
                     coords:fallbackPositionObject});
      });
    });
    
    return def.promise;
  }
  
  return {
    init:function(nom){
      if(!isInit){
        isInit=true;
        _nom=nom;
        interval=$interval(this.setLoc,delay);
      }
    },
    setLoc:function(nom){
      getCurrentPosition().then(function(pos){
      geo.$set('Ming',[pos.coords.latitude,pos.coords.longitude]);
      },function(err){
        console.log(err);
      })
    },
  };

})*/;
