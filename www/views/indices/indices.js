angular.module('Indices',[])
.controller('IndicesCtrl',function($scope,$stateParams,$state,indice,IndicesService,$window){
  
 $scope.indice=indice;
  var reponses=[20,'462271','150077','22221','00621',10];
  $scope.hideContent=false;
  $scope.indiceUrl='views/indices/indice'+indice+'.html';
  $scope.reponse='';
  $scope.valid=false;
  $scope.comm={};
  
  IndicesService.getSyncedChat().$bindTo($scope,'comm').then(function(){
    if(!angular.isDefined($scope.comm.chat)){
      $scope.comm.chat=''; 
    }
     if(!angular.isDefined($scope.comm.chat)){
      $scope.comm.reponse=''; 
    }
    $scope.comm.question=indice;
  });
  
  
  $scope.submitReponse=function(rep){
    if(rep==reponses[indice-1] && !$scope.valid){
      $scope.valid=true;
      $scope.comm.chat='';
      nextQuestion();
    }else{
      $scope.message="Mauvaise réponse. Essaie encore!";
    }
    $scope.comm.reponse='';
  };
  
  function nextQuestion(){
    IndicesService.setNextIndice()
      .then(function(){
        $window.location.reload();
    });
  }
  
});

/**/